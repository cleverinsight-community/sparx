pandas==0.19.2
geopy==1.11.0
scipy==0.18.1
numpy==1.13.1
python-dateutil==2.6.0
scikit-learn==0.18.1
blaze==0.11.0